package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorArmas {
	private ArrayList<Arma> listaArmas;
	private ArrayList<Armero> listaArmeros;
	
	public GestorArmas() {
		listaArmas = new ArrayList<Arma>();
		listaArmeros = new ArrayList<Armero>();
	}
	
	public boolean existeArmero(String dni) {
		for (Armero armero: listaArmeros) {
			if(armero != null && armero.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	
	public void altaArmero(String dni, String nombre, String especialidad) {
		if(!existeArmero(dni)) {
			Armero nuevoArmero = new Armero(dni,nombre, especialidad);
			listaArmeros.add(nuevoArmero);
		}
		else {
			System.out.println("Ya existe ese armero");
		}
	}
	
	public void listarArmeros() {
		for (Armero armero: listaArmeros) {
			System.out.println(armero.toString());
		}
	}
	
	public void eliminarArmero(String dni) {
		Iterator<Armero> iteradorArmeros = listaArmeros.iterator();
		
		while(iteradorArmeros.hasNext()) {
			Armero armero = iteradorArmeros.next();
			if(armero.getDni().equals(dni)) {
				iteradorArmeros.remove();
			}
		}
	}
	
	public Armero buscarArmero(String dni) {
		for (Armero armero: listaArmeros) {
			if(armero != null && armero.getDni().equals(dni)) {
				return armero;
			}
		}
		return null;
	}
	
	public Armero buscarArmeroEspecialidad(String especialidad) {
		for(int i = 0; i < listaArmeros.size(); i++) {
			Armero armero = listaArmeros.get(i);
			if(armero != null && armero.getEspecialidad().equals(especialidad)) {
				return armero;
			}
		}
		return null;
	}
	
	public boolean comprobarArmeroEspecialidad(String especialidad) {
		if (buscarArmeroEspecialidad(especialidad) != null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public Armero buscarArmeroDesocupado() {
		for(Armero armero: listaArmeros) {
			int contador = 0;
			for(Arma arma: listaArmas) {
				
				if(arma.getArmeroArma().equals(armero)) {
					break;
				}else {
					contador++;
				}
				
				if (contador == listaArmas.size()) {
					return armero;
				}
				
			}
		}
		return null;
	}
	
	public boolean existeArma(String nombreArma) {
		for(Arma arma: listaArmas) {
			if(arma != null && arma.getNombre().equals(nombreArma)) {
				return true;
			}
		}
		return false;
	}
	
	public void altaArma(String nombreArma, String tipo, String cliente, double precio) {
		if(!existeArma(nombreArma)) {
			Arma nuevaArma = new Arma(nombreArma, tipo, cliente, precio);
			listaArmas.add(nuevaArma);
		}
		else {
			System.out.println("El arma ya existe");
		}
	}
	
	public void eliminarArma(String nombreArma) {
		for(Arma arma: listaArmas) {
			if(arma != null && arma.getNombre().equals(nombreArma)) {
				arma = null;
			}
		}
	}
	
	public Arma buscarArma(String nombreArma) {
		for (Arma arma: listaArmas) {
			if(arma != null && arma.getNombre().equals(nombreArma)) {
				return arma;
			}
		}
		return null;
	}
	
	public void listarArmas() {
		for(Arma arma: listaArmas) {
			System.out.println(arma.toString());
		}
	}
	
	public void listarArmasPorTipo(String tipo) {
		for(Arma arma: listaArmas) {
			if(arma != null && arma.getTipo().equals(tipo)) {
				System.out.println(arma.toString());
			}
		}
	}
	
	public void listarArmasPorArmero(String dni) {
		for (Arma arma: listaArmas) {
			if(arma != null && arma.getArmeroArma().getDni().equals(dni)) {
				System.out.println(arma.toString());
			}
		}
	}
	
	public void asignarArmero(String dni, String nombreArma) {
		for(Arma arma: listaArmas) {
			if(arma != null && arma.getNombre().equals(nombreArma)) {
				arma.setArmeroArma(buscarArmero(dni));
			}
		}
	}
	
	public void asignarArmeroDesocupado(String nombreArma) {
		buscarArma(nombreArma).setArmeroArma(buscarArmeroDesocupado());
		
		if(buscarArma(nombreArma).getArmeroArma() == null) {
			System.out.println("No habia armeros desocupados, el arma no se asignara");
		}
	}
	
	public void asignarArmeroEspecialidad(String nombreArma) {
		buscarArma(nombreArma).setArmeroArma(buscarArmeroEspecialidad(buscarArma(nombreArma).getTipo()));
		
		if(buscarArma(nombreArma).getArmeroArma() == null) {
			System.out.println("No habia armeros especializados en ese tipo de arma, el arma no se asignara");
		}
	}
	
	
	
	
	
	
	
	
	
}
