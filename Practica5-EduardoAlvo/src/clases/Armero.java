package clases;

public class Armero {
	private String dni;
	private String especialidad;
	private String nombre;
	
	public Armero(String dni, String nombre, String especialidad) {
		this.dni = dni;
		this.especialidad = especialidad;
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Armero [dni=" + dni + ", especialidad=" + especialidad + ", nombre=" + nombre + "]";
	}
	
	
}
