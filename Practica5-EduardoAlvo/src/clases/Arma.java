package clases;

public class Arma {
	
	private String nombre;
	private String tipo;
	private String cliente;
	private double precio;
	private Armero armeroArma;
	

	public Arma (String nombre, String tipo, String cliente, double precio) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.cliente = cliente;
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Armero getArmeroArma() {
		return armeroArma;
	}

	public void setArmeroArma(Armero armeroArma) {
		this.armeroArma = armeroArma;
	}

	@Override
	public String toString() {
		return "Arma [nombre=" + nombre + ", tipo=" + tipo + ", cliente=" + cliente + ", precio=" + precio
				+ ", armeroArma=" + armeroArma + "]";
	}
	
	
	
	
}
