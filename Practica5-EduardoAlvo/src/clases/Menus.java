package clases;

import java.util.Scanner;

public class Menus {
	GestorArmas gestor = new GestorArmas();
	Scanner input = new Scanner (System.in);
	public int opcion;
	
	public Menus() {
		
	}
	
	
	public void ejecutarMenu(){
	
	do {
		System.out.println("-------------------------------------");
		System.out.println("|          MENU PRINCIPAL           |");
		System.out.println("|         ----------------          |");
		System.out.println("| 1-Dar de alta un armero           |");
		System.out.println("| 2-Listar armeros                  |");
		System.out.println("| 3-Eliminar armero                 |");
		System.out.println("| 4-Buscar armero por dni           |");
		System.out.println("| 5-Comprobar armero especialidad   |");
		System.out.println("| 6-Dar de alta un arma             |");
		System.out.println("| 7-Listar armas                    |");
		System.out.println("| 8-Eliminar arma                   |");
		System.out.println("| 9-Buscar arma por el nombre       |");
		System.out.println("| 10-Listar armas por tipo          |");
		System.out.println("| 11-Listar armas por armero        |");
		System.out.println("| 12-Asignar armero                 |");
		System.out.println("| 13-Asignar armero desocupado      |");
		System.out.println("| 14-Asignar armero especializado   |");
		System.out.println("| 15-Salir del programa             |");
		System.out.println("-------------------------------------");
		
		opcion = input.nextInt();
		
		switch(opcion) {
		
		case 1:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el dni del nuevo armero |");
			System.out.println("-------------------------------------");
			String dni1 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|       Introduce su nombre         |");
			System.out.println("-------------------------------------");
			String nombreArmero1 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|     Introduce su especialidad     |");
			System.out.println("-------------------------------------");
			String especialidad1 = input.nextLine();
			gestor.altaArmero(dni1, nombreArmero1, especialidad1);
			System.out.println("-------------------------------------");
			System.out.println("| Armero dado de alta correctamente |");
			System.out.println("-------------------------------------");
			break;
			
		case 2:
			gestor.listarArmeros();
			break;
			
		case 3:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|    Introduce el dni del armero    |");
			System.out.println("-------------------------------------");
			String dni2 = input.nextLine();
			gestor.eliminarArmero(dni2);
			System.out.println("-------------------------------------");
			System.out.println("|  Armero eliminado correctamente   |");
			System.out.println("-------------------------------------");
			break;
			
		case 4:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|   Introduce el dni del armero     |");
			System.out.println("-------------------------------------");
			String dni3 = input.nextLine();
			if (gestor.buscarArmero(dni3) != null) {
			System.out.println(gestor.buscarArmero(dni3).toString());
			} else {
				System.out.println("El armero que buscas no existe");
			}
			break;
			
		case 5:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|     Introduce la especialidad     |");
			System.out.println("-------------------------------------");
			String especialidad2 = input.nextLine();
			if(gestor.comprobarArmeroEspecialidad(especialidad2)) {
				System.out.println("Hay armeros especializados");
			}
			else {
				System.out.println("No hay armeros especializados");
			}
			break;
			
		case 6:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma1 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el tipo del arma        |");
			System.out.println("-------------------------------------");
			String tipo1 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el cliente del arma     |");
			System.out.println("-------------------------------------");
			String cliente1 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el precio del arma      |");
			System.out.println("-------------------------------------");
			double precio1 = input.nextDouble();
			gestor.altaArma(nombreArma1, tipo1, cliente1, precio1);
			System.out.println("-------------------------------------");
			System.out.println("| Arma creada correctamente         |");
			System.out.println("-------------------------------------");
			break;
			
		case 7:
			gestor.listarArmas();
			break;
			
		case 8:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma2 = input.nextLine();
			gestor.eliminarArma(nombreArma2);
			System.out.println("-------------------------------------");
			System.out.println("| Arma eliminada correctamente      |");
			System.out.println("-------------------------------------");
			break;
			
		case 9:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma3 = input.nextLine();
			if (gestor.buscarArma(nombreArma3) != null) {
			System.out.println(gestor.buscarArma(nombreArma3).toString());
			} else {
				System.out.println("El arma que buscas no existe");
			}
			break;
			
		case 10:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el tipo del arma        |");
			System.out.println("-------------------------------------");
			String tipo2 = input.nextLine();
			gestor.listarArmasPorTipo(tipo2);
			break;
			
		case 11:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|   Introduce el dni del armero     |");
			System.out.println("-------------------------------------");
			String dni4 = input.nextLine();
			gestor.listarArmasPorArmero(dni4);
			break;
			
		case 12:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("|   Introduce el dni del armero     |");
			System.out.println("-------------------------------------");
			String dni5 = input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma4 = input.nextLine();
			if (gestor.buscarArma(nombreArma4) != null && gestor.buscarArmero(dni5) != null) {
			gestor.asignarArmero(dni5, nombreArma4);
			System.out.println("-------------------------------------");
			System.out.println("| Armero asignado correctamente     |");
			System.out.println("-------------------------------------");
			} else {
				System.out.println("Alguno de los elementos no existen");
			}
			break;
			
		case 13:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma5 = input.nextLine();
			if (gestor.buscarArma(nombreArma5) != null) {
			gestor.asignarArmeroDesocupado(nombreArma5);
			} else {
				System.out.println("No existe esa arma");
			}
			break;
			
		case 14:
			input.nextLine();
			System.out.println("-------------------------------------");
			System.out.println("| Introduce el nombre del arma      |");
			System.out.println("-------------------------------------");
			String nombreArma6 = input.nextLine();
			if (gestor.buscarArma(nombreArma6) != null) {
			gestor.asignarArmeroEspecialidad(nombreArma6);
			} else {
				System.out.println("El arma no existe");
			}
			break;
			
		case 15:
			System.out.println("Saliendo del programa...");
			break;
			
		default:
			System.out.println("-------------------------------------");
			System.out.println("|   Introduce una opcion correcta   |");
			System.out.println("-------------------------------------");
			
		}
		
	} while(opcion != 15);
	
	input.close();


	}
}

